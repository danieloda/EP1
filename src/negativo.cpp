#include "negativo.hpp"
#include <cstdio>
#include <cstdlib>

using namespace std;

Negativo::Negativo(){

	setLargura(0);
	setAltura(0);
	setIntensidade_pixel(0);
}

Negativo::Negativo(int largura, int altura, int intensidade_pixel){
	setLargura(largura);
	setAltura(altura);
	setIntensidade_pixel(intensidade_pixel);
}

void Negativo::savePixel(ofstream &novoarquivo){

	int largura, altura, intensidade_pixel;

	largura = getLargura();
	altura = getAltura();
	intensidade_pixel = getIntensidade_pixel();

	int i=0, j=0;


	for(i = 0; i < altura; i++)
	{
		for(j = 0; j < largura; j++)
		{
		matrizR[i][j] = intensidade_pixel - matrizR[i][j];
		novoarquivo << matrizR[i][j];

		matrizG[i][j] = intensidade_pixel - matrizG[i][j];
		novoarquivo << matrizG[i][j];

		matrizB[i][j] = intensidade_pixel - matrizB[i][j];
		novoarquivo << matrizB[i][j];
		}
	}


}
Negativo::~Negativo(){

}
