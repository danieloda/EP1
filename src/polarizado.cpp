#include "polarizado.hpp"

using namespace std;

Polarizado::Polarizado(){

	setLargura(0);
	setAltura(0);
	setIntensidade_pixel(0);

}

Polarizado::Polarizado(int largura, int altura, int intensidade_pixel){
	setLargura(largura);
	setAltura(altura);
	setIntensidade_pixel(intensidade_pixel);

}

void Polarizado::savePixel(ofstream &novoarquivo){

	int largura, altura, intensidade_pixel;

	largura = getLargura();
	altura = getAltura();
	intensidade_pixel = getIntensidade_pixel();

	int R, G, B;

	int i, j;

	for(i = 0; i < altura; i++)
	{
		for(j = 0; j < largura; j++)
		{
		R = matrizR[i][j];
		if(R > intensidade_pixel/2)
		matrizR[i][j] = intensidade_pixel;
		else
		matrizR[i][j] = 0;

		G = matrizG[i][j];
		if(G > intensidade_pixel/2)
		matrizG[i][j] = intensidade_pixel;
		else
		matrizG[i][j] = 0;

		B = matrizB[i][j];
		if(B > intensidade_pixel/2)
		matrizB[i][j] = intensidade_pixel;
		else
		matrizB[i][j] = 0;

		novoarquivo << (char)matrizR[i][j];
		novoarquivo << (char)matrizG[i][j];
		novoarquivo << (char)matrizB[i][j];


		}

	}


}
Polarizado::~Polarizado(){

}
