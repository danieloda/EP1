#include "media.hpp"

using namespace std;

Media::Media(){

	setLargura(0);
	setAltura(0);
	setIntensidade_pixel(0);
}

Media::Media(int largura, int altura, int intensidade_pixel){
	setLargura(largura);
	setAltura(altura);
	setIntensidade_pixel(intensidade_pixel);
}

void Media::savePixel(ofstream &novoarquivo){

	int largura, altura;

	largura = getLargura();
	altura = getAltura();

	fstream ArquivoDestino;

	int tamanhofiltro;
	cout << "Insira o tamanho do filtro" << endl;
	do{cin >> tamanhofiltro;if(tamanhofiltro < 0){cout << "Insira valor positivo"<<endl;}}while(tamanhofiltro < 0);
	ifstream file_read;

	int R = 0, G = 0, B = 0, contador = 0;

	tamanhofiltro = tamanhofiltro/2;

	for(int i=0; i<altura; i++){
		for(int j=0; j<largura; j++){

			for(int a=-tamanhofiltro; a<=tamanhofiltro; a++){
				for(int b=-tamanhofiltro; b<=tamanhofiltro; b++){

					if((i+a) >= 0 && (a+i) < altura){
						if((j+b)>=0 && (j+b) < largura){
							R += (int)matrizR[i+a][j+b];
							G += (int)matrizG[i+a][j+b];
							B += (int)matrizB[i+a][j+b];
							contador++;
						}
					}

				}
			}
			R = R/contador;
			G = G/contador;
			B = B/contador;
			matrizR[i][j] = (unsigned char)R;
			matrizG[i][j] = (unsigned char)G;
			matrizB[i][j] = (unsigned char)B;

			novoarquivo.write((char*)&matrizR[i][j], sizeof(unsigned char));
			novoarquivo.write((char*)&matrizG[i][j], sizeof(unsigned char));
			novoarquivo.write((char*)&matrizB[i][j], sizeof(unsigned char));




		}
	}
}
