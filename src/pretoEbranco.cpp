#include "pretoEbranco.hpp"

using namespace std;

PretoeBranco::PretoeBranco(){

	setLargura(0);
	setAltura(0);
	setIntensidade_pixel(0);

}

PretoeBranco::PretoeBranco(int largura, int altura, int intensidade_pixel){
	setLargura(largura);
	setAltura(altura);
	setIntensidade_pixel(intensidade_pixel);

}

void PretoeBranco::savePixel(ofstream &novoarquivo){

	int largura, altura, intensidade_pixel;
	largura = getLargura();
	altura = getAltura();
	intensidade_pixel = getIntensidade_pixel();

int i, j;
int cinza;
for(i = 0; i < altura; i++){
		for(j = 0; j < largura; j++){
		cinza = (0.299*matrizR[i][j]) + (0.587*matrizG[i][j]) + (0.144*matrizB[i][j]);
		if(cinza > intensidade_pixel){
		matrizR[i][j] = intensidade_pixel;
		matrizG[i][j] = intensidade_pixel;
		matrizB[i][j] = intensidade_pixel;
		novoarquivo << matrizR[i][j];
		novoarquivo << matrizG[i][j];
		novoarquivo << matrizB[i][j];
}
		else{
		matrizR[i][j] = (char)cinza;
		matrizG[i][j] = (char)cinza;
		matrizB[i][j] = (char)cinza;
		novoarquivo << matrizR[i][j];
		novoarquivo << matrizG[i][j];
		novoarquivo << matrizB[i][j];
}

}

}


}
PretoeBranco::~PretoeBranco(){

}
