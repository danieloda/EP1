#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "imagem.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "pretoEbranco.hpp"
#include "media.hpp"

using namespace std;

int menu();

int main() {

	int valor;

  Imagem *imagem = new Imagem();
	Negativo *negativo = new Negativo();
	Polarizado *polarizado = new Polarizado();
	PretoeBranco *pretoEbranco = new PretoeBranco();
  Media *media = new Media();
  ofstream novoarquivo;

  do{
		valor = menu();
		switch (valor) {
		case 0:
			cout << "     Programa encerrado" << endl;
			break;
		case 1:
			imagem->saveDados(novoarquivo);
			cout << "ERRO!!"	<< endl;
			break;
		case 2:
			negativo->saveDados(novoarquivo);
			break;
    case 3:
      polarizado->saveDados(novoarquivo);
      break;
    case 4:
      pretoEbranco->saveDados(novoarquivo);
      break;
    case 5:
      media->saveDados(novoarquivo);
      break;
    default:
			cout << "ERRO!! Caminho invalido" << endl;
			cout << "Enter para retornar ao menu" << endl;
			cin.clear();
			cin.ignore(10000,'\n');
			cin.ignore(1);
		break;
		}
	}while(valor != 0);

	return 0;
}
int menu() {
	int opcao_menu = 5;
	system("clear");//Limpa a Tela
	cout << "*******************************************************" << endl;
	cout << "*             Universidade de Brasilia                *" << endl;
	cout << "*          Disciplina:Orientacao a Objetos            *" << endl;
	cout << "*     Aluno:Daniel Ashton Oda - 15/0008121      *" << endl;
	cout << "*******************************************************" << endl;
	cout << "               Digite a opcao desejada: " << endl;
	cout << "      1 -> Normal"      << endl;
	cout << "      2 -> Negativo" << endl;
  cout << "      3 -> Polarizado" << endl;
  cout << "      4 -> Preto e Branco" << endl;
  cout << "      5 -> Filtro de Media:" << endl;
  cout << "      0 -> Sair " << endl;
	cout << "      Opçao:  ";
	cin >> opcao_menu;
	cout << endl;
	return opcao_menu;
}
