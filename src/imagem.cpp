#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

Imagem::Imagem(){

	formato = "";
	largura = 0;
	altura = 0;
	intensidade_pixel = 255;

	matrizR = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	matrizR[i] = new unsigned char[largura];
  matrizG = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	matrizG[i] = new unsigned char[largura];
  matrizB = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	matrizB[i] = new unsigned char[largura];

}
Imagem::Imagem(string formato, int largura, int altura, int intensidade_pixel){
	this->formato = formato;
	this->largura = largura;
	this->altura = altura;
	this->intensidade_pixel = intensidade_pixel;
}
void Imagem::saveDados(ofstream &novoarquivo){

	ifstream arquivo;
	string nomedoarquivo;
	cout << "Digite o nome da imagem para aplicar o filtro: ";
	cin >> nomedoarquivo;
	nomedoarquivo = "./doc/" + nomedoarquivo + ".ppm";

	arquivo.open(nomedoarquivo.c_str(), ios::binary);

	if(!arquivo.is_open()){

	cout << "Arquivo não encontrado!" << endl;
}
	else{
	string formato, comentario, comentarioaux;
	int largura, altura, intensidade_pixel;

	getline(arquivo,formato);

	if(formato != "P6")
{
	cout << "Formato de imagem inválida!" << endl;
	return;
}

	while(1)
	{

	getline(arquivo,comentario);

	if(comentario[0] != '#')
	{
	cout << "Armazenando Comentario: " << comentario << endl;
	int tamanho = comentario.length()+1;
	cout << "Tamanho do Comentário: " << tamanho << endl;
	arquivo.seekg(-tamanho,ios_base::cur);
	arquivo >> largura;
	cout << "Largura: " << largura << endl;
	arquivo >> altura;
	cout << "Altura: " << altura << endl;
	arquivo >> intensidade_pixel;
	cout << "Intensidade máxima: " << intensidade_pixel << endl;
	break;
	}

	}
	setFormato(formato);
	setLargura(largura);
	setAltura(altura);
	setIntensidade_pixel(intensidade_pixel);

	arquivo.seekg(1,ios_base::cur);

	string nomearquivonovo;

	cout << "Digite o nome da nova imgagem: " << endl;
	cin >> nomearquivonovo;

	nomearquivonovo = "./doc/" + nomearquivonovo + ".ppm";

	while(nomearquivonovo == nomedoarquivo)
	{
	cout << "Nome de arquivo inválido. Por favor, insira um novo nome: ";
	cin >> nomearquivonovo;
	}

	novoarquivo.open(nomearquivonovo.c_str(),ios::binary);

	novoarquivo << formato << endl;
	novoarquivo << largura << " " << altura << endl;
	novoarquivo << intensidade_pixel << endl;

	int j, k;
	char r, g, b;

	matrizR = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	matrizR[i] = new unsigned char[largura];
	matrizG = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	matrizG[i] = new unsigned char[largura];
	matrizB = new unsigned char*[altura];
	for(int i = 0; i < altura; i++)
	matrizB[i] = new unsigned char[largura];

	for(j = 0; j < altura; j++)
	{
		for(k = 0; k < largura; k++)
		{
		arquivo.get(r);
		arquivo.get(g);
		arquivo.get(b);
		matrizR[j][k] = r;
		matrizG[j][k] = g;
		matrizB[j][k] = b;
}

}

}
	savePixel(novoarquivo);
	arquivo.close();
	novoarquivo.close();
}

void Imagem::savePixel(ofstream &novoarquivo){

  int **R, **G, **B;

	R = new int*[altura];
	for(int i = 0; i < altura; i++)
	R[i] = new int[largura];
  G = new int*[altura];
	for(int i = 0; i < altura; i++)
	G[i] = new int[largura];
  B = new int*[altura];
	for(int i = 0; i < altura; i++)
	B[i] = new int[largura];
  int i, j;
  for(i = 0; i < altura; i++)
	{
		for(j = 0; j < largura; j++)
		{
		R[i][j] = (unsigned int)matrizR[i][j];
		novoarquivo << (char)R[i][j];
		G[i][j] = (unsigned int)matrizG[i][j];
		novoarquivo << (char)G[i][j];
		B[i][j] = (unsigned int)matrizB[i][j];
		novoarquivo << (char)B[i][j];
		}
	}


}

string Imagem::getFormato(){
	return formato;
}

void Imagem::setFormato(string formato){
	this->formato = formato;
}

int Imagem::getLargura(){
	return largura;
}

void Imagem::setLargura(int largura){
	this->largura = largura;
}

int Imagem::getAltura(){
	return altura;
}

void Imagem::setAltura(int altura){
	this->altura = altura;
}

int Imagem::getIntensidade_pixel(){
	return intensidade_pixel;
}

void Imagem::setIntensidade_pixel(int intensidade_pixel){
	this->intensidade_pixel = intensidade_pixel;
}
Imagem::~Imagem(){
}
