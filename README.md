# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

Instruções para compilação do programa:

    Utilize o comando make para compilar o programa:
    $ make
    Para executar:
    $ make run
    As imagens de formato PPM estão diponíveis para teste na pasta doc
    No menu, digite a opção referente ao tipo de imagem que deseja filtrar;
    Digite o nome da imagem; > Exemplos:
        imagem1
        imagem2
    Digite o nome da imagem filtrada; > Exemplos:
	imagem1pretoEbranco
	imagem2polarizada
    	
