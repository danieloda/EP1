#ifndef PRETOEBRANCO_HPP
#define PRETOEBRANCO_HPP

#include <iostream>
#include "imagem.hpp"

class PretoeBranco : public Imagem {

public:

	PretoeBranco();
	~PretoeBranco();
	PretoeBranco(int largura, int altura, int intensidade_pixel);
	void savePixel(ofstream &novoarquivo);



};

#endif
