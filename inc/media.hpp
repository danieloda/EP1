#ifndef MEDIA_HPP
#define MEDIA_HPP

#include <iostream>
#include "imagem.hpp"

class Media : public Imagem {

public:

	Media();
	~Media();
	Media(int largura, int altura, int intensidade_pixel);
	void savePixel(ofstream &novoarquivo);

};

#endif
