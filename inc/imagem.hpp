#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>
#include <fstream>

using namespace std;

class Imagem {

private:

     string formato;
     int largura;
     int altura;
     int intensidade_pixel;

public:

      ofstream novoarquivo;
      unsigned char **matrizR, **matrizG, **matrizB;
      Imagem();
      Imagem(string formato, int largura, int altura, int intensidade_pixel);
     ~Imagem();

     string getFormato();
     void setFormato(string formato);

     int getLargura();
     void setLargura(int largura);

     int getAltura();
     void setAltura(int altura);

     int getIntensidade_pixel();
     void setIntensidade_pixel(int intensidade);

     void saveDados(ofstream &novoarquivo);
     virtual	void savePixel(ofstream &novoarquivo);


};
#endif
