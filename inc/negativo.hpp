#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include <iostream>
#include "imagem.hpp"

class Negativo : public Imagem {

public:

	Negativo();
	~Negativo();
	Negativo(int largura, int altura, int intensidade_pixel);
	void savePixel(ofstream &novoarquivo);

};

#endif
