#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP

#include <iostream>
#include "imagem.hpp"

class Polarizado : public Imagem{

public:

	Polarizado();
	~Polarizado();
	Polarizado(int largura, int altura, int intensidade_pixel);
	void savePixel(ofstream &novoarquivo);



};

#endif
